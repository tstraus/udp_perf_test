#include <array>
#include <cstdint>
#include <locale>

#include "CLI/CLI.hpp"
#include "Poco/Clock.h"
#include "fmt/core.h"

#include "get_run_state.hpp"

using namespace Poco::Net;

uint16_t port = 1234;

CLI::App app{"Receives UDP packets as fast as it can"};

const auto* portOption = app.add_option("-p,--port", port, "Port to receive data on. Default: 1234");

int main(int argc, char** argv)
{
    std::locale::global(std::locale("en_US.UTF-8"));

    CLI11_PARSE(app, argc, argv);

    const auto [expectedCount, size] = get_run_state(port);

    // set up the buffer
    std::vector<int8_t> buffer;
    buffer.reserve(size);
    for (uint64_t i = 0; i < size; i++)
    {
        buffer.push_back(1);
    }

    auto socket = DatagramSocket(SocketAddress(port), true);

    Poco::Clock start;
    uint64_t count = 0;
    while (count < expectedCount)
    {
        socket.receiveBytes(buffer.data(), (int)buffer.size());
        if (count == 0)
        {
            start = Poco::Clock();
        }
        count++;
    }
    const auto elapsed = (double)(Poco::Clock() - start) / 1000000; // convert to seconds
    const auto pps = (uint64_t)((double)count / elapsed);
    const auto bandwidth = ((double)count * (double)size * 8.0 / (1024 * 1024)) / elapsed;

    fmt::println("--- done ---");
    fmt::println("received {:L} packets in {:.2f} ms", count, elapsed * 1000);
    fmt::println("{:L} pps, {:.2f} Mb/s", pps, bandwidth);

    return 0;
}
