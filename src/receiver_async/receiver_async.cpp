#include <array>
#include <atomic>
#include <cstdint>
#include <iostream>
#include <limits>
#include <locale>
#include <thread>
#include <utility>

#include "CLI/CLI.hpp"
#include "Poco/Clock.h"
#include "asio.hpp"
#include "fmt/core.h"

#include "get_run_state.hpp"

using namespace asio;
using namespace asio::ip;

class MultiReceiver
{
  public:
    MultiReceiver(uint16_t _port,
                  std::function<void(size_t)> _callback,
                  int _num_sockets = 1,
                  int _num_threads = 1,
                  int _num_contexts = 1)
        : callback(std::move(_callback))
    {
        io_contexts.resize(_num_contexts);
        worker_threads.resize(_num_threads);
        sockets.resize(_num_sockets);

        if (_num_contexts == _num_threads && _num_contexts == _num_sockets)
        { // dedicated thread and io_service for each socket
            for (int i = 0; i < _num_contexts; i++)
            {
                io_contexts[i] = std::make_unique<io_context>(_num_threads);
                auto& context = *io_contexts[i];
                works.push_back(make_work_guard(context));

                worker_threads[i] = std::make_unique<std::thread>([&]() { context.run(); });

                int one = 1;
                auto socket = std::make_unique<udp::socket>(context);
                socket->open(udp::v4());
#ifdef WIN32
                setsockopt(socket->native_handle(), SOL_SOCKET, SO_REUSEADDR, (const char*) & one, sizeof(one));
#else
                setsockopt(socket->native_handle(), SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &one, sizeof(one));
#endif
                socket->bind(udp::endpoint(udp::v4(), _port));
                sockets[i] = std::move(socket);
            }
        }

        else if (_num_contexts == 1)
        { // tunable sockets and threads shared by a single io_service
            io_contexts[0] = std::make_unique<io_context>(_num_threads);
            auto& context = *io_contexts[0];
            works.push_back(make_work_guard(context));

            for (int i = 0; i < _num_threads; i++)
            {
                worker_threads[i] = std::make_unique<std::thread>([&]() { context.run(); });
            }

            for (int i = 0; i < _num_sockets; i++)
            {
                int one = 1;
                auto socket = std::make_unique<udp::socket>(context);
                socket->open(udp::v4());
#ifdef WIN32
                setsockopt(socket->native_handle(), SOL_SOCKET, SO_REUSEADDR, (const char*)&one, sizeof(one));
#else
                setsockopt(socket->native_handle(), SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &one, sizeof(one));
#endif
                socket->bind(udp::endpoint(udp::v4(), _port));
                sockets[i] = std::move(socket);
            }
        }

        else
        {
            throw std::runtime_error(fmt::format("Invalid resources, sockets({}), threads({}), io_services({}), # of "
                                                 "io_services must be 1 or match the others",
                                                 _num_sockets,
                                                 _num_threads,
                                                 _num_contexts));
        }

        fmt::println("MultiReceiver created with sockets({}), threads({}), io_service({})",
                     _num_sockets,
                     _num_threads,
                     _num_contexts);

        buffers.resize(_num_sockets);

        for (int i = 0; i < _num_sockets; i++)
        {
            auto& b = buffers[i];
            auto& s = *sockets[i];
            s.async_receive(buffer(b.data(), b.size()), [&](error_code err, size_t size) { handler(s, size, b); });
        }
    }

    ~MultiReceiver()
    {
        for (auto& context : io_contexts)
        {
            context->stop();
        }

        for (auto& worker : worker_threads)
        {
            worker->join();
        }
    }

  private:
    inline void handler(udp::socket& socket,
                        size_t size,
                        std::array<int8_t, std::numeric_limits<uint16_t>::max()>& buff)
    {
        callback(size);
        socket.async_receive(buffer(buff.data(), buff.size()),
                             [&](error_code err, size_t size) { handler(socket, size, buff); });
    }

    std::function<void(size_t)> callback;

    std::vector<std::unique_ptr<io_context>> io_contexts;
    std::vector<asio::executor_work_guard<io_context::executor_type>> works;
    std::vector<std::unique_ptr<std::thread>> worker_threads;
    std::vector<std::unique_ptr<udp::socket>> sockets;
    std::vector<std::array<int8_t, std::numeric_limits<uint16_t>::max()>> buffers;
};

uint16_t port = 1234;
int num_sockets = 1;
int num_threads = 1;
int num_contexts = 1;

CLI::App app{"Receives UDP packages using ASIO as fast as it can"};

const auto* portOption = app.add_option("-p,--port", port, "Port to receive data on. Default: 1234");
const auto* numSocketsOption =
    app.add_option("-s,--sockets", num_sockets, "Number of sockets to recive on. Default: 1");
const auto* numThreadsOption =
    app.add_option("-t,--threads", num_threads, "Number of threads to receive with. Default: 1");
const auto* numContextsOption = app.add_option(
    "-c,--contexts",
    num_contexts,
    "Number of ASIO contexts to use, NOTE: must be set to same number as threads and sockets if not 1. Default: 1");

int main(int argc, char** argv)
{
    try
    {
        std::locale::global(std::locale("en_US.UTF-8"));

        CLI11_PARSE(app, argc, argv);

        const auto [expectedCount, size] = get_run_state(port);

        Poco::Clock end;
        std::atomic<uint64_t> count = 0;

        MultiReceiver receiver(
            port,
            [&](size_t size) {
                count++;
                if (count == expectedCount)
                {
                    end = Poco::Clock();
                }
            },
            num_sockets,
            num_threads,
            num_contexts);

        Poco::Clock start;

        while (count != expectedCount)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        const auto elapsed = (double)(Poco::Clock() - start) / 1000000; // convert to seconds
        const auto pps = (uint64_t)((double)count / elapsed);
        const auto bandwidth = ((double)count * (double)size * 8.0 / (1024 * 1024)) / elapsed;

        fmt::println("--- done ---");
        fmt::println("received {:L} packets in {:.2f} ms", count.load(), elapsed * 1000);
        fmt::println("{:L} pps, {:.2f} Mb/s", pps, bandwidth);

        return 0;
    }
    catch (const std::exception& err)
    {
        fmt::println("ERROR: {}", err.what());

        return 1;
    }
}
