#pragma once

#include <array>
#include <locale>
#include <tuple>

#include "Poco/Net/DatagramSocket.h"
#include "fmt/core.h"

using namespace Poco::Net;

std::pair<uint64_t, uint64_t> get_run_state(uint16_t port)
{
    auto socket = DatagramSocket(SocketAddress(port), true);

    // get what to expect
    std::array<uint64_t, 2> receiverInfo;
    while (true)
    {
        if (socket.receiveBytes(receiverInfo.data(), sizeof(receiverInfo)) == sizeof(receiverInfo))
        {
            break;
        }
    }

    const auto& expectedCount = receiverInfo[0];
    const auto& size = receiverInfo[1];

    fmt::println("receiving {:L} {} byte packets on {}", expectedCount, size, port);

    return {expectedCount, size};
}
