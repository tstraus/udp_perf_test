project (utils)

add_library (${PROJECT_NAME} INTERFACE get_run_state.hpp)

target_link_libraries (${PROJECT_NAME}
    INTERFACE fmt::fmt-header-only
    INTERFACE Poco::Net
)

target_include_directories (${PROJECT_NAME} 
    INTERFACE ${CMAKE_CURRENT_LIST_DIR}
)
