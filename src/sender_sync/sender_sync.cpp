#include <array>
#include <chrono>
#include <cstdint>
#include <locale>
#include <thread>
#include <vector>

#include "CLI/CLI.hpp"
#include "Poco/Clock.h"
#include "Poco/Net/DNS.h"
#include "Poco/Net/DatagramSocket.h"
#include "fmt/core.h"

using namespace std::chrono_literals;
using namespace Poco::Net;

uint64_t count = 10000;
uint64_t size = 512;
std::string destinationIP = "127.0.0.1";
std::string destination;
uint16_t destinationPort = 1234;
uint16_t numThreads = 1;
bool singleSocket = false;

CLI::App app{"Sends UDP packets as fast as it can"};

auto* countOption = app.add_option("-c,--count", count, "Number of messages to send. Default: 10000");
auto* sizeOption = app.add_option("-s,--size", size, "Size of message to send in bytes. Default: 512");
auto* ipOption = app.add_option("-i,--ip", destinationIP, "IP Address to send data to. Default: '127.0.0.1'");
auto* destinationOption = app.add_option("-d,--dest", destination, "Host to send data to. Default: 'localhost'");
auto* portOption = app.add_option("-p,--port", destinationPort, "Port to send data to. Default: 1234");
auto* threadsOption = app.add_option("-t,--threads", numThreads, "Number of threads to send from. Default: 1");
auto* singleSocketOption = app.add_flag("--single", singleSocket, "Force all send threads to share the same socket");

std::vector<uint64_t> counters;
std::vector<std::vector<int8_t>> sendBuffers;

std::atomic<bool> startSignal = false;

void send(uint64_t thread_index, auto& sharedSocket)
{
    const auto& buffer = sendBuffers[thread_index];
    auto& counter = counters[thread_index];
    const auto numToSend = count / numThreads;

    auto localSocket = DatagramSocket(SocketAddress(), true, true);
    const auto destAddr = SocketAddress(destinationIP, destinationPort);

    auto& socket = singleSocket ? sharedSocket : localSocket;

    while (!startSignal)
    {
        ;
    }

    // send the data
    for (uint64_t i = 0; i < numToSend; i++)
    {
        socket.sendTo(buffer.data(), (int)buffer.size(), destAddr);
        counter++;
    }
}

int main(int argc, char** argv)
{
    std::locale::global(std::locale("en_US.UTF-8"));

    destinationOption->excludes(ipOption);
    ipOption->excludes(destinationOption);
    CLI11_PARSE(app, argc, argv);

    // if dest arg is set, do a DNS lookup on it
    if (!destination.empty())
    {
        try
        {
            destinationIP = DNS::resolve(destination).addresses()[0].toString();
        }
        catch (const std::exception& e)
        {
            fmt::println("DNS Lookup error: {}", e.what());
            return 1;
        }
    }

    // adjust send count for number of threads
    count = (count / numThreads) * numThreads;

    fmt::println("sending {:L} {} byte packets to {}:{}", count, size, destinationIP, destinationPort);

    auto socket = DatagramSocket(SocketAddress(), true, true);

    // tell the receiver what to expect
    std::array<uint64_t, 2> receiverInfo;
    receiverInfo[0] = count;
    receiverInfo[1] = size;
    socket.sendTo(receiverInfo.data(), sizeof(receiverInfo), SocketAddress(destinationIP, destinationPort));

    // set up thread specific items
    for (uint64_t i = 0; i < numThreads; i++)
    {
        std::vector<int8_t> buffer;
        buffer.reserve(size);
        for (uint64_t i = 0; i < size; i++)
        {
            buffer.push_back(1);
        }
        sendBuffers.push_back(buffer);
        counters.push_back({0});
    }

    // start the threads
    std::vector<std::thread> threads;
    for (uint64_t i = 0; i < numThreads; i++)
    {
        threads.emplace_back([i, &socket]() { send(i, socket); });
    }

    // wait for receiver to get it
    std::this_thread::sleep_for(10ms);

    // tell them to go
    startSignal = true;
    const auto start = Poco::Clock();

    // wait for them to finish
    for (auto& thrd : threads)
    {
        thrd.join();
    }

    uint64_t sendCount = 0;
    for (const auto& counter : counters)
    {
        sendCount += counter;
    }

    const auto elapsed = (double)(Poco::Clock() - start) / 1000000; // convert to seconds
    const auto pps = (uint64_t)((double)sendCount / elapsed);
    const auto bandwidth = ((double)sendCount * (double)size * 8.0 / (1024 * 1024)) / elapsed;

    fmt::println("--- done ---");
    fmt::println("sent {:L} packets in {:.2f} ms", sendCount, elapsed * 1000);
    fmt::println("{:L} pps, {:.2f} Mb/s", pps, bandwidth);

    return 0;
}
